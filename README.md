# EMongo

一个仿造Tp的链式操作的mongodb类，但是参数尽量使用原mongodb形式的参数，减少因转换指令需要消耗的性能，以适应生产环境需要。目前至支持部分指令和参数，持续更新中。。。

qq:1136223237

几点说明：
+ 作者使用环境php7.1+mongodb extension1.4 + mongodb3.6.3。对于不同版本的兼容问题，暂时没计划(基本功能还没完成来着。。。),其他版本的可以自己按照该代码做调整修改，不难的
+ 使用中建议添加namespace
+ 使用时期望能保留笔者署名，尊重原创
+ 以下实例仅包含部分使用方法，更多使用方法请自行阅读源码
+ 欢迎加qq交流合作，笔者希望能有志同爱好者共同开发更多功能和衍生开源产品以提高自身同时服务大众
+ 欢迎各种拍砖
+ 鄙视伸手党，喜欢至少给个赞鼓励一下笔者

实例
```php
$db = new /EFrame/Mongo("10.10.56.192:27017", 'test', 'root', 'root', 'test');
//$db->insert(['name' => 'tissue', 'msg' => 'world!', 'time' => time(), 'info' => ['age' => 20, 'sex' => 'female']]);
$res = $db->where(['name' => 'funco'])->limit(1, 2)->field(['name', 'time'])->select();
foreach ($res as $col) {
    print_r($col);
}
echo '### nest' . PHP_EOL;
$res = $db->where(['info.sex' => 'male'])->limit(1, 2)->field(['name', 'time'])->select();
foreach ($res as $col) {
    print_r($col);
}
echo '### compare' . PHP_EOL;
$res = $db->where(['info.age' => ['$lt' =>22]])->select();
foreach ($res as $col) {
    print_r($col);
}
```